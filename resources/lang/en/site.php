<?php
return [
    /*MENU*/
    'home' => 'Home',
    'service' => 'Service',
        /*Sub Menu*/
        'sub_service1' => 'Fusion Focus',
        'sub_service2' => 'Relaxation Massage',
<<<<<<< HEAD
        'sub_service3' => 'Therapeutic Massage',
=======
        'sub_service2.1' => 'Relaxation',
        'sub_service3' => 'Therapeutic Massage',
        'sub_service3.1' => 'Therapeutic',
>>>>>>> 476630172c36dfcf6968273407a765009861ca9c

    'package' => 'Package',
    'contact' => 'Contact',

    /*Home Page*/
    'welcome' => 'SAWASDEE KA',
    'welcome_detail' => 'Welcome to Siam Smile Thai Massage, a massage center designed from the best Thai massage traditions have to offer.',
    'slow_gan' => 'Change the way you feel',

    /* Heading each of pages */
    'our_service' => 'Our Services',
    'our_cus_say' => 'What our clients say',
    
    /*Services*/
    
    /* ETC */
    'TSS' => 'Siam Smile Thai Massage',
    'our_service_detail' => 'Whether you seek relaxation, flexibility, or pain relief, we can help. Friendly, welcoming and authentically Thai, we are massage specialists.',
    'our_package' => 'Our Packages',

    /*Fusion Focus*/
    'item1' => 'HEAD MASSAGE',
    'detail_it1' => 'Excellent to relief “Migraines” pain and stress and also improve flexibility and blood circulation within the head.',

    'item2' => 'BACK, HEAD, NECK & SHOULDERS  MASSAGE',
    'detail_it2' => 'For the backaches and all the part on top resulting from long hours spent at the desk, this massage serves as a perfect relief to iron out tension and pain. Guests can choose to enjoy this massage with or without oil.',

    'item3' => 'FOOT REFLEXOLOGY MASSAGE',
    'detail_it3' => 'A relaxing massage concentrating on the pressure points to rejuvenate tired, focused on your feet and half legs (knees to feet, toes), worn muscles from the heel to the tips of the toes.',

    'item4' => 'ABDOMINAL MASSAGE',
    'detail_it4' => 'Increases blood flow within the abdomen to increase oxygen to the organs. Stimulates the body’s natural detoxification process.',

    /*Relaxation*/
    'item5' => 'THAI AROMATHERAPY MASSAGE',
    'detail_it5' => 'This Massage is the ideal choice of care and soothing results. A light, calming massage using a blend of essential oils, which works the whole body. This indulgent treatment leaving you feeling deeply relaxed and re-charged. If you prefer NO AROMA, Please! Let us know before service.',

    'item6' => 'THAI HERBAL HEAT MASSAGE',
    'detail_it6' => 'An excellent treatment using a blend of traditional Thai herbs. A heated parcel of traditional Thai herbs is administered directly to the body, penetrating deep into the muscle restoring balance and alleviating pain.
                    Oil is used in this treatment.',

    'item7' => 'HOT STONE MASSAGE',
    'detail_it7' => 'This heavenly treatment uses heated volcanic basalt stones and oil. The combination of heat and massage reduces muscular tension, soothes the nervous system and relieves stress and emotional fatigue.',

    'item8' => 'COCONUT OIL MASSAGE',
    'detail_it8' => 'Coconut oil is packed with benefits as it is healing and nourishing to the skin. It soothes and hydrates dry skin and can help treat many skin conditions. The ultimate natural vitamin E. Good for dry skin.',

    'item9' => 'PREGNANCY MASSAGE',
    'detail_it9' => 'Service is provided by experienced massage therapist who is specialized in post and prenatal massage. Baby oil or natural carrier oil without color and odor is used during the session.
                    •	Pregnancy Massage is usually gentler than many other massage techniques.
                    •	It focuses more on techniques that aid the circulatory and lymphatic systems, to help ensure a healthy flow of blood to mother and baby.
                    
                    Pregnancy Massage available only after 3 – 7 months.',

    /*THERAPEUTIC*/
    'item10' => 'TRADITIONAL THAI  MASSAGE',
    'detail_it10' => 'We can call “Thai Yoga”. The movements are based on stretching and pressure points. A perfect mix of subtle stretching with rhythmic massaging and compressions to balance the body system and stimulate energy flow. No oil is used and wearing a cotton pajamas.',

    'item11' => 'THAI HEALING TOUCH',
    'detail_it11' => 'This massage is Advanced Thai Therapeutic Massage. Acupressure and deep pressure techniques are applied. This treatment is used of balm or oil or none at all, depends on the techniques of Therapist.
                        A medium to Strong Massage recommended for age especially those who have to undergo surgery at the hospital from various symptoms such as knee pain, hips, legs, bone overlapping, office syndrome, etc.',

    'item12' => 'SPORT MASSAGE',
    'detail_it12' => 'Deeply revitalizing massage. Our therapists using technique of thumbs, palms pressure with firm strokes. The intensive strokes work to loosen and relax the  muscles. Oil is used in this treatment.',

    /* Packages */
    'pack_a' => 'Package A',
    'pack_b' => 'Package B',
    'pack_c' => 'Package C',
    'pack_d' => 'Package D',

    /*Price start at*/
    'price_sa' => 'starts at only',

    'addrs' => 'Rua De Dona Estefánia, N.155A,
                1000-126 Lisboa, Portugal
                (Just walking 500 m. from Metro
                Saldanha)',
    'open' =>   'Opening Hour',
    'open1' => 'Open Everyday: 10 AM. – 20 PM.',
    'tel' => 'Tel: (+351) 911 962 809',




];

